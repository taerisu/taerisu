<h2 align="center">Friends</h2>
<p align="center">
<a href="https://github.com/askiphy"><img src="https://img.shields.io/badge/-Sergey%20Fomchukov-000000?style=for-the-badge" alt="@askiphy"></a>
<a href="https://github.com/mckoda09"><img src="https://img.shields.io/badge/-mckoda09-000000?style=for-the-badge" alt="@mckoda09"></a>
<a href="https://github.com/itsLameni"><img src="https://img.shields.io/badge/-Evgeny-000000?style=for-the-badge" alt="@Evgeny"></a> 
<a href="https://github.com/orzklv"><img src="https://img.shields.io/badge/-Sokhibjon%20Orzikulov-000000?style=for-the-badge" alt="@Yūri Katsuki"></a>
<a href="https://github.com/chelbaev-danil"><img src="https://img.shields.io/badge/-chelbaev%20danil-000000?style=for-the-badge" alt="@chelbaev-danil"></a>
<a href="https://github.com/akumarujon"><img src="https://img.shields.io/badge/-Akumarujon-000000?style=for-the-badge" alt="@akumarujon"></a>
</p>

<h2 align="center">Contact me</h2>
<p align="center">
<a href="https://tapni.su"><img src="https://img.shields.io/badge/-tapni.su-000000?style=for-the-badge&logo=nextdotjs" alt="Website"></a>
<a href="https://tapni.su/telegram"><img src="https://img.shields.io/badge/-Telegram-000000?style=for-the-badge&logo=Telegram" alt="Telegram"></a>
<a href="https://tapni.su/github"><img src="https://img.shields.io/badge/-Github-000000?style=for-the-badge&logo=Github" alt="Github"></a>
<a href="https://tapni.su/x"><img src="https://img.shields.io/badge/-X-000000?style=for-the-badge&logo=X" alt="X"></a>
<a href="https://tapni.su/discord"><img src="https://img.shields.io/badge/-Discord:%20tapnisu-000000?style=for-the-badge&logo=Discord" alt="Discord"></a>
<a href="https://tapni.su/email"><img src="https://img.shields.io/badge/-Mail-000000?style=for-the-badge&logo=gmail" alt="Mail"></a>
</p>
